<?php

namespace Dorigo\MailChimp\Settings;

if(is_admin()) {
    add_action('admin_menu', __NAMESPACE__.'\\menu');
    add_action('admin_init', __NAMESPACE__.'\\register_settings');
}

function menu() {
    add_options_page('MailChimp Settings','MailChimp','manage_options','dorigo_mailchimp',__NAMESPACE__.'\\settings_output');
}


function settings() {
    $settings = [
        [
            'default' => '',
            'name' => 'dorigo_mailchimp_api_key',
            'label' => 'API Key',
        ],

        [
            'name' => 'dorigo_mailchimp_list',
            'label' => 'Default List',
        ],

        [
            'name' => 'dorigo_mailchimp_success',
            'label' => 'Success Message',
            'default' => 'Thanks for subscribing!',
        ],
    ];

    return apply_filters('Dorigo/MailChimp/Settings', $settings);
}


function register_settings() {
    $settings = settings();

    foreach($settings as $setting) {
        register_setting('dorigo_mailchimp', $setting['name']);
    }
}


function settings_output() {
    $settings = settings();

    echo '<div class="wrap">';
        echo '<h2>MailChimp Settings</h2>';

        echo '<form action="options.php" method="post">';
            settings_fields('dorigo_mailchimp');

            echo '<p>You get your API key by logging into your <a href="https://www.mailchimp.com" target="_blank">MailChimp</a> account, going to <strong>Profile</strong>, going to <strong>Extras</strong> then clicking <strong>API Keys</strong>.</p>';

            echo '<table class="form-table">';
                echo '<tbody>';

                foreach($settings as $setting) {
                    echo '<tr>';
                        echo '<th scope="row">';
                            echo '<label for="'.$setting['name'].'">'.$setting['label'].'</label>';
                        echo '</th>';

                        echo '<td>';
                            $value = get_option($setting['name']);

                            if($setting['name'] === 'dorigo_mailchimp_list') {

                                echo '<select name="'.$setting['name'].'" id="'.$setting['name'].'">';
                                    echo '<option value="" disabled'.(!$value?' selected':'').'>Please Select a List</option>';

                                    if(get_option('dorigo_mailchimp_api_key')) {
                                        if(!isset($api)) { $api = new \Dorigo\MailChimp(); }

                                        foreach($api->lists() as $list) {
                                            echo '<option value="'.$list['id'].'"'.($list['id'] === $value ? ' selected' :'').'>&nbsp;&nbsp;'.$list['name'].'</option>';
                                        }
                                    }

                                echo '</select>';

                            } else {

                                echo '<input type="text" name="'.$setting['name'].'" id="'.$setting['name'].'" value="'.($value ?: (isset($setting['default']) ? $setting['default'] : '')).'" class="regular-text ltr">';

                            }

                        echo '</td>';
                    echo '<tr>';

                }

                echo '</tbody>';
            echo '</table>';


            submit_button();
        echo '</form>';

    echo '</div>';
}
