# MailChimp

A tiny WordPress plugin to let you easily add a MailChimp signups to your website.

## Installation

Either download the plugin and extract into your WordPress plugins directory, or get it via composer.

### Using Composer

First, add the Dorigo Packages repository to your composer installation. This is password protected, so make sure you have your [auth.json](https://getcomposer.org/doc/articles/http-basic-authentication.md) file, too.

Then, run `composer require dorigo/mailchimp`.

## Setting Up

After the plugin is installed and activated, you set up the required settings.

Go to **Settings » Mailchimp**.

Enter your API Key, then save the changes. Then, you’ll be able to pick your default list.

You can also set a success message, that will be displayed to the user after signing up.

## Example Code

    <form action="<?= \Dorigo\Mailchimp::subscribeUrl(); ?>" method="get">
        <?= \Dorigo\Mailchimp::formFields(); ?>
    
        <label>
            Your Email Address
            <input type="email" name="email" class="text-input" required>
        </label>

        <button type="submit"><?= __('Sign Up'); ?></button>
    </form>

The plugin will add an AJAX event to submitting this form. During submission, it add a `loading` class to the form element, and a will append a div with the class `form-loading` to the form.

After submission, it will remove the loading class and element, and append a notice with the class `form-notice` to the form. This notice will also include the class `form-notice--success` or `form-notice--error`.