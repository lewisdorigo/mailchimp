<?php
/**
 * Plugin Name:       MailChimp
 * Plugin URI:        https://bitbucket.org/lewisdorigo/mailchimp
 * Description:       MailChimp API for Wordpress
 * Version:           2.0.0
 * Author:            Lewis Dorigo
 * Author URI:        https://dorigo.co/
 */

namespace Dorigo\MailChimp;

if(!defined('ABSPATH')) exit; // Exit if accessed directly


if(is_dir(dirname(__FILE__).DIRECTORY_SEPARATOR.'vendor')) {
    require_once('vendor/autoload.php');
}

require_once('settings.php');
require_once('mailchimp.class.php');




function enqueue_scripts() {
    wp_enqueue_script('dorigo-mailchimp', plugins_url("mailchimp.js", __FILE__), ['jquery'], false, true);
}

add_action('wp_enqueue_scripts', __NAMESPACE__.'\\enqueue_scripts');
