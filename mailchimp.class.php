<?php

namespace Dorigo;

class MailChimp {
    private $apiKey;
    private $list;
    private $successMessage;
    private $mailchimp;
    private static $api;
    public static $ajaxAction = 'mailchimp-subscribe';

    private static function Instance() {
        if(self::$api === null) {
            self::$api = new \Dorigo\MailChimp();
        }

        return self::$api;
    }

    public function __construct() {
        if($key = get_option('dorigo_mailchimp_api_key')) {
            $this->apiKey = $key;
        } else {
            self::error('There is no API Key');
        }

        $this->list = get_option('dorigo_mailchimp_list');
        $this->successMessage = get_option('dorigo_mailchimp_success') ?: 'Thanks for subscribing!';

        $this->mailchimp = new \Mailchimp($this->apiKey);

        self::$api = $this;
    }


    public function lists() {
        try {
            $lists = $this->mailchimp->lists->getList([], 0, 100);
        } catch (Mailchimp_Error $e) {
            self::error($e);
        }

        return $lists['data'];
    }

    public static function subscribeUrl() {
        return admin_url('admin-ajax.php');
    }

    public static function formFields() {
        $fields = [
            '<input type="hidden" name="action" value="'.self::$ajaxAction.'">',
            '<input type="hidden" name="mailchimp-form" value="true">',
        ];

        return implode(PHP_EOL, $fields);
    }

    public static function subscribe() {
        $api = self::Instance();

        $list   = isset($_GET['list'])   ? $_GET['list']   : self::$api->list;
        $email  = isset($_GET['email'])  ? $_GET['email']  : false;

        try {
            $result = $api->mailchimp->lists->subscribe(
                $list,
                ['email' => $email]
            );
        } catch (Mailchimp_Error $e) {
            self::error($e);
        }

        self::showResult([
            'status'  => 'success',
            'code'    => 200,
            'message' => self::$api->successMessage,
        ]);
    }

    private static function error($result) {
        trigger_error("Failed with error: ".$result->getCode().". ".$result->getMessage());



        self::showResult([
            'status'  => 'error',
            'message' => $result->getMessage(),
            'code'    => $result->getCode()
        ], $result->getCode());
    }

    private static function showResult($object = [], $status = 200) {
        header('Content-Type: application/json');
        http_response_code($status);

        echo json_encode($object);
        die();
    }
}



add_action('wp_ajax_'.MailChimp::$ajaxAction, __NAMESPACE__.'\\MailChimp::subscribe');
add_action('wp_ajax_nopriv_'.MailChimp::$ajaxAction, __NAMESPACE__.'\\MailChimp::subscribe');
